
## ОПИСАНИЕ

1) src - исходный код и изображения для плагинов на Chrome и Firefox
    * chrome/flvto_chrome_plugin/ - скрипт плагина (btnInjecter.js), файл параметров (manifest.json) и иконка
    * firefox/Youtube MP3 Plugin/, firefox/Youtube to audio converter/, firefox/YouTube to MP3 Button/ - каталог с иконками двух разрешений, 3 скриншота для стора, скрипт плагина (ytflvtomp3.js) и файл параметров (manifest.json). "version" в файле manifest.json должна быть изменена при каждом обновлении плагина
2) build - сгенерированные файлы плагинов
    * Chrome - архив хромовского формата crx (flvto_chrome_plugin.crx) и ключ приложения формата pem (flvto_chrome_plugin.pem)
    * Firefox - сгенерированные архивы для каждого из трех сервисов (Youtube_MP3_Plugin.xpi, Youtube_to_audio_converter.xpi, YouTube_to_MP3_Button.xpi)
   
   
## Команда для БИЛДА архивов Firefox плагинов
В соответствующих каталогах:
```
zip -r Youtube_to_audio_converter.xpi *
zip -r Youtube_MP3_Plugin.xpi *
zip -r YouTube_to_MP3_Button.xpi *
```

## Алгоритм для БИЛДА архива Chrome плагина
В браузере Chrome - меню настроек (три точечки справа вверху) - доп.инструменты - расширения - отметить галочкой сверху Режим разработчика - нажать на кнопку "Упаковать расширение" - в качестве корневого каталога расширения выбрать папку conv_addons/src/chrome/flvto_chrome_plugin. Два файла crx и pem появятся в том же каталоге


## ТЕСТИРОВАНИЕ плагинов (в соответствующем браузере)
* Chrome - меню настроек - доп.инструменты - расширения - отметить галочкой сверху Режим разработчика - нажать на кнопку Загрузить распакованное расширение - в открывшемся окне выбрать нужную папку или собранный архив плагина - на ютубе под любым видео появится красная кнопка Convert to MP3
* Firefox - ввести в адресной строке about:debugging - нажать кнопку Загрузить временное расширение - выбрать нужный архив формата xpi - на ютубе под роликами будут кнопки (3х разных цветов для разных сервисов)


## DEPLOY плагинов (в соответствующем браузере)
* Chrome - файл crx необходимо залить на cdn сервер
* Firefox - https://addons.mozilla.org/en-US/developers/addons зайти на страницу нужного плагина и кликнуть "Upload New Version"